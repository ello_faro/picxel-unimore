from django.contrib import admin
from userauth.models import Profile


# Register your models here.

class SettingAdmin(admin.ModelAdmin):
    list_display = ('user', 'first_name', 'last_name', 'email')


admin.site.register(Profile, SettingAdmin)
