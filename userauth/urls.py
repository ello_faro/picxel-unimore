from django.urls import path

from userauth.forms import UserLoginForm
from userauth.views import UserProfileView, RegisterUserView, CompleteRegistrationView, EditProfileView, \
    ProfileStatsView, ChangePasswordView, follow, searchprofile, followersmodal, followingmodal
from django.contrib.auth import views as auth_views

urlpatterns = [

    # User authentication
    path('sign-up/', RegisterUserView.as_view(), name="signup"),
    path('sign-in/', auth_views.LoginView.as_view(template_name="userauth/sign-in.html", redirect_authenticated_user=True, authentication_form=UserLoginForm), name="signin"),
    path('sign-out/', auth_views.LogoutView.as_view(template_name="userauth/sign-out.html"), name="signout"),

    # User complete profile
    path('completeregistration/', CompleteRegistrationView.as_view(), name="completeregistration"),

    # User statistics
    path('statistics/', ProfileStatsView.as_view(), name="profilestats"),

    # User edit profile
    path('editprofile/', EditProfileView.as_view(), name="editprofile"),

    # User change password
    path('changepassword/', ChangePasswordView.as_view(), name="changepassword"),

    # User profile
    path('<username>/', UserProfileView.as_view(), name="profile"),

    # User saved posts
    path('<username>/saved-posts', UserProfileView.as_view(), name="favourite"),

    # User follow functionality
    path('<username>/follow', follow, name="follow"),

    #User search
    path('search', searchprofile, name="searchprofile"),

    #User followers
    path('<username>/followers', followersmodal, name="followers-modal"),

    #User following
    path('<username>/following', followingmodal, name="following-modal")

]
