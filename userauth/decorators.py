from functools import wraps
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, redirect
from django.urls import reverse

from userauth.forms import EditProfileForm
from userauth.models import Profile


def ajax_login_required(view_func):
    @wraps(view_func)
    def wrapper(request, *args, **kwargs):
        if request.user.is_authenticated:
            profile = Profile.objects.filter(user=request.user).exists()
            if profile:
                return view_func(request, *args, **kwargs)

        return HttpResponse(status=401)

    return wrapper


def complete_profile_or_anonymous_user_required(view_func):
    @wraps(view_func)
    def wrapper(request, *args, **kwargs):
        if request.user.is_authenticated:
            profile = Profile.objects.filter(user=request.user).exists()
            if not profile:
                return redirect(reverse("completeregistration"))

        return view_func(request, *args, **kwargs)

    return wrapper


def logged_user_without_profile(view_func):
    @wraps(view_func)
    def wrapper(request, *args, **kwargs):
        if request.user.is_authenticated:
            profile = Profile.objects.filter(user=request.user).exists()
            if not profile:
                return view_func(request, *args, **kwargs)

        return redirect(reverse("index"))

    return wrapper


def deny_anonymous_user(view_func):
    @wraps(view_func)
    def wrapper(request, *args, **kwargs):
        if request.user.is_authenticated:
            profile = Profile.objects.filter(user=request.user).exists()
            if profile:
                return view_func(request, *args, **kwargs)
            else:
                return redirect(reverse("completeregistration"))

        return redirect(reverse("signin"))

    return wrapper
