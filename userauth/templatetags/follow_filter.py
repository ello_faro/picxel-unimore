from django import template
from django.shortcuts import get_object_or_404
from userauth.models import Profile

register = template.Library()


def check_follow(logged_user, user):
    profile_obj = get_object_or_404(Profile, user=logged_user)
    if profile_obj.following.contains(user):
        return True
    else:
        return False


register.filter('check_follow', check_follow)
