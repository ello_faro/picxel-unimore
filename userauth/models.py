from datetime import date

from django.db import models
from django.contrib.auth.models import User

from notifications.models import Notification, Statistics
from post.models import Post
from PIL import Image
from django.utils import timezone

# Create your models here.

# Upload dei file dell'utente in una directory specifica


def user_directory_path(instance, filename):
    return 'user_{0}/{1}'.format(instance.user.id, filename)


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name="profile")
    first_name = models.CharField(max_length=50, null=False, blank=False)
    last_name = models.CharField(max_length=50, null=False, blank=False)
    email = models.EmailField(max_length=90, null=False, blank=False, unique=True, error_messages={'unique': "A user with that email address already exists."})
    location = models.CharField(max_length=50, null=True, blank=True)
    url = models.URLField(max_length=250, null=True, blank=True)
    profile_info = models.TextField(max_length=150, null=True, blank=True)
    created = models.DateField(auto_now_add=True)
    profile_image = models.ImageField(upload_to=user_directory_path, null=False, blank=True, verbose_name="Profile image", default="default-user.png")
    favourites = models.ManyToManyField(Post, blank=True)
    following = models.ManyToManyField(User, related_name="Users_followed", blank=True)
    followers = models.ManyToManyField(User, related_name="Followers_of_this_user", blank=True)

    def __str__(self):
        return f'{self.user.username} - Profile'

    def get_followers_count(self):
        return self.followers.count()

    def get_following_count(self):
        return self.following.count()

    def get_posts_count(self):
        return Post.objects.filter(user=self.user).count()

    def get_posts(self):
        return Post.objects.filter(user=self.user).order_by('-date').all()

    def get_favourite_posts(self):
        return self.favourites.all()

    def follow_status(self, user):
        if user.is_anonymous:
            return False
        else:
            return self.followers.contains(user)

    def follow(self, user):
        if self.following.contains(user):
            self.following.remove(user)
            user.profile.followers.remove(self.user)
            return False
        else:
            self.following.add(user)
            user.profile.followers.add(self.user)
            return True

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)

        if self.profile_image:
            img = Image.open(self.profile_image.path)
            if img.height > 300 or img.width > 300:
                output_size = (300, 300)
                img.thumbnail(output_size)
                img.save(self.profile_image.path)
