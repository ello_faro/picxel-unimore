import unittest

from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import reverse

from userauth.models import Profile


# Create your tests here.


class ProfileCreationTests(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(username='testuser', password='12345')
        self.profile = Profile.objects.create(user=self.user, first_name="test", last_name="profile", email="test@test.com")

    # consistenza architetturale
    def testObj(self):
        self.assertTrue(
            isinstance(self.profile, Profile),
            "profile non è un Profile"
        )

    # coerenza funzionale: metodi invocabili
    def test_methods(self):
        self.assertTrue(
            callable(self.profile.get_following_count),
            "profile non ha il metodo get_following_count"
        )
        self.assertTrue(
            callable(self.profile.get_followers_count),
            "profile non ha il metodo get_followers_count"
        )
        self.assertTrue(
            callable(self.profile.__str__),
            "profile non ha il metodo __str__"
        )
        self.assertTrue(
            callable(self.profile.get_favourite_posts),
            "profile non ha il metodo get_favourite_posts"
        )
        self.assertTrue(
            callable(self.profile.get_posts),
            "profile non ha il metodo get_posts"
        )
        self.assertTrue(
            callable(self.profile.get_posts_count),
            "profile non ha il metodo get_posts_count"
        )
        self.assertTrue(
            callable(self.profile.follow_status),
            "profile non ha il metodo follow_status"
        )
        self.assertTrue(
            callable(self.profile.follow),
            "profile non ha il metodo follow"
        )
        self.assertTrue(
            callable(self.profile.save),
            "profile non ha il metodo save"
        )

    def test_profile_without_name(self):
        # robustezza: input non validi
        self.assertTrue(
            self.profile.first_name != '',
            "Manca il nome del profilo"
        )

    def test_profile_without_last_name(self):
        # robustezza: input non validi
        self.assertTrue(
            self.profile.last_name != '',
            "Manca il cognome del profilo"
        )

    def test_profile_without_email(self):
        # robustezza: input non validi
        self.assertTrue(
            self.profile.email != '',
            "Manca l'email del profilo"
        )

    # coerenza funzionale: risultati attesi dei metodi get/set
    def test_method_str(self):
        self.assertEqual(
            self.profile.__str__(),
            self.user.username + ' - Profile',
            "Lo username del profilo non è stato recuperato correttamente."
        )
    def test_method_get_followers_count(self):
        self.assertEqual(
            self.profile.get_followers_count(),
            0,
            "Il numero di followers del profilo non è stato recuperato correttamente."
        )
    def test_method_get_following_count(self):
        self.assertEqual(
            self.profile.get_following_count(),
            0,
            "Il numero di utenti seguiti dal profilo non è stato recuperato correttamente."
        )
    def test_method_get_posts_count(self):
        self.assertEqual(
            self.profile.get_posts_count(),
            0,
            "Il numero di post pubblicati dal profilo non è stato recuperato correttamente."
        )

    def tearDown(self):
        del self.profile
        del self.user


class ProfileViewsTest(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(username='testuser', password='12345')
        self.profile = Profile.objects.create(user=self.user, first_name="test", last_name="profile", email="test@test.com")

    def test_get_profile_page(self):
        self.assertEqual(
            self.client.get(reverse('profile', args=[self.profile.user.username])).status_code,
            200,
            "La risposta non è 200"
        )


if __name__ == '__main__':
    unittest.main()