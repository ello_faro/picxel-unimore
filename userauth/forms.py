from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Field, HTML
from django.contrib.auth.forms import UserCreationForm, PasswordChangeForm
from django.contrib.auth.models import User

from userauth.models import Profile
from django.contrib.auth.forms import AuthenticationForm, UsernameField
from django import forms


class UserLoginForm(AuthenticationForm):
    username = UsernameField(widget=forms.TextInput(attrs={'class': 'input', 'placeholder': 'Username'}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'input', 'placeholder': 'Password'}))

    def __init__(self, *args, **kwargs):
        super(UserLoginForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.form_show_labels = False
        self.helper.layout = Layout(Field('username', 'password'),
                                    HTML("""<button class="form-btn" type='submit'>Sign In</button>"""))

    class Meta:
        model = User
        fields = ['username', 'password']


class UserRegisterForm(UserCreationForm):
    username = forms.CharField(widget=forms.TextInput(attrs={'class': 'input', 'placeholder': 'Username'}),
                               required=True)
    password1 = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'input', 'placeholder': 'Enter password'}),
                                required=True)
    password2 = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'input', 'placeholder': 'Confirm password'}),
                                required=True)

    def __init__(self, *args, **kwargs):
        super(UserRegisterForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.form_show_labels = False
        self.helper.form_method = 'POST'
        self.helper.layout = Layout(Field('username', 'password1', 'password2'),
                                    HTML("""<button class="form-btn" type='submit'>Sign Up</button>"""))

    class Meta:
        model = User
        fields = ['username', 'password1', 'password2']


class EditProfileForm(forms.ModelForm):
    profile_image = forms.ImageField(required=False)
    first_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'input', 'placeholder': 'First name'}),
                                 required=True)
    last_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'input', 'placeholder': 'Last name'}),
                                required=True)
    email = forms.EmailField(widget=forms.EmailInput(attrs={'class': 'input', 'placeholder': 'Email'}), required=True)
    location = forms.CharField(widget=forms.TextInput(attrs={'class': 'input', 'placeholder': 'Location'}),
                               required=False)
    url = forms.URLField(widget=forms.TextInput(attrs={'class': 'input', 'placeholder': 'URL'}), required=False)
    profile_info = forms.CharField(widget=forms.TextInput(attrs={'class': 'input', 'placeholder': 'Bio'}),
                                   required=False)

    def __init__(self, *args, **kwargs):
        super(EditProfileForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.layout = Layout(
            Field('profile_image', 'first_name', 'last_name', 'email', 'location', 'url', 'profile_info'),
            HTML("""<button class="btn btn-success btn-lg mt-5" type='submit'>Save changes</button>"""))

    class Meta:
        model = Profile
        fields = ['profile_image', 'first_name', 'last_name', 'email', 'location', 'url', 'profile_info']


class ChangePasswordForm(PasswordChangeForm):
    old_password = forms.PasswordInput()
    new_password1 = forms.PasswordInput()
    new_password2 = forms.PasswordInput()

    def __init__(self, *args, **kwargs):
        super(ChangePasswordForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.form_show_labels = True
        self.helper.layout = Layout(Field('old_password', 'new_password1', 'new_password2'),
                                    HTML("""<button class="btn btn-success btn-lg mt-5" type='submit'>Save changes</button>"""))

    class Meta:
        model = User
        fields = ['old_password', 'new_password1', 'new_password2']