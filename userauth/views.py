from datetime import date

from django.contrib.auth.views import PasswordChangeView
from django.db.models import Q
from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth import authenticate, login, update_session_auth_hash
from django.contrib.auth.models import User
from django.urls import resolve, reverse
import sweetify
from django.utils.decorators import method_decorator
from django.views.generic import DetailView, CreateView, UpdateView, TemplateView

from notifications.models import Notification, Statistics
from post.models import Post
from userauth.decorators import complete_profile_or_anonymous_user_required, deny_anonymous_user, \
    logged_user_without_profile
from userauth.forms import EditProfileForm, UserRegisterForm, ChangePasswordForm
from userauth.models import Profile


# Create your views here.
class RegisterUserView(CreateView):
    template_name = 'userauth/sign-up.html'
    form_class = UserRegisterForm

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect('index')
        else:
            return super().get(request, *args, **kwargs)

    def form_valid(self, form):
        form.save()
        new_user = authenticate(username=form.cleaned_data['username'], password=form.cleaned_data['password1'])
        login(self.request, new_user)
        sweetify.success(self.request, 'Success!', text='Good job, you successfully registered to Picxel!',
                         persistent='Complete your profile information')
        return redirect('completeregistration')


@method_decorator(logged_user_without_profile, name='dispatch')
class CompleteRegistrationView(CreateView):
    template_name = 'complete-profile.html'
    form_class = EditProfileForm
    success_url = "/"

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)


@method_decorator(complete_profile_or_anonymous_user_required, name='dispatch')
class UserProfileView(DetailView):
    template_name = 'profile.html'
    context_object_name = 'profile'

    def get_object(self, queryset=None):
        user = User.objects.get(username=self.kwargs['username'])
        return get_object_or_404(Profile, user=user)

    def get_context_data(self, **kwargs):
        context = super(UserProfileView, self).get_context_data(**kwargs)

        # Post da mostrare
        url_name = resolve(self.request.path).url_name
        if url_name == 'profile':
            posts = context['object'].get_posts()
        else:
            posts = context['object'].get_favourite_posts()

        context['follow_status'] = context['object'].follow_status(self.request.user)
        context['posts'] = posts
        return context


@deny_anonymous_user
def follow(request, username):
    logged_user = request.user
    user = User.objects.get(username=username)

    if logged_user.profile.follow(user):
        if Notification.objects.filter(sender=logged_user, user=user, type=3).count() == 0:
            Notification.objects.create(sender=logged_user, user=user, type=3)
            if Statistics.objects.filter(sender=logged_user, user=user, type=2, date=date.today()).count() == 0:
                Statistics.objects.create(sender=logged_user, user=user, type=2)
        else:
            Notification.objects.filter(sender=logged_user, user=user, type=3).delete()
    else:
        Notification.objects.filter(sender=logged_user, user=user, type=3).delete()

    return HttpResponseRedirect(reverse('profile', args=[user]))


@method_decorator(deny_anonymous_user, name='dispatch')
class EditProfileView(UpdateView):
    form_class = EditProfileForm
    template_name = 'editprofile.html'

    def get_object(self, queryset=None):
        return Profile.objects.get(user=self.request.user)

    def form_valid(self, form):
        if form.instance.profile_image == "":
            form.instance.profile_image = 'default-user.png'
        form.save()
        return redirect('/user/' + self.request.user.username)


@method_decorator(deny_anonymous_user, name='dispatch')
class ChangePasswordView(PasswordChangeView):
    template_name = 'changepassword.html'
    form_class = ChangePasswordForm

    def form_valid(self, form):
        form.save()
        update_session_auth_hash(self.request, form.user)
        sweetify.success(self.request, 'Success!', text='Good job, you successfully changed your password!',
                            persistent='Return to Picxel')
        return redirect('profile', self.request.user.username)


def searchprofile(request):
    query = request.GET.get('q')
    if query:
        users = User.objects.filter(Q(username__startswith=query))[:5]
        user_list = []
        for user in users:
            if hasattr(user, 'profile'):
                user_obj = {'username': user.username, 'img': user.profile.profile_image.url,
                            "name": user.profile.first_name + " " + user.profile.last_name}
                user_list.append(user_obj)
        return JsonResponse(user_list, safe=False)
    else:
        user_list = []
        return JsonResponse(user_list, safe=False)


@method_decorator(deny_anonymous_user, name='dispatch')
class ProfileStatsView(TemplateView):
    template_name = 'statistics.html'

    def get_context_data(self, **kwargs):
        data = {}

        likes = list(Statistics.objects.filter(user=self.request.user, type=1).values())
        followers = list(Statistics.objects.filter(user=self.request.user, type=2).values())
        posts = list(Statistics.objects.filter(user=self.request.user, type=3).values())
        following = list(Statistics.objects.filter(sender=self.request.user, type=2).values())

        data['likes'] = graphdata(likes)
        data['followers'] = graphdata(followers)
        data['posts'] = graphdata(posts)
        data['following'] = graphdata(following)

        return data


def graphdata(collection):
    dates = []
    values = {}
    for value in collection:
        dates.append(str(value['date']))
    for date in dates:
        values[date] = values.get(date, 0) + 1
    return values


def followersmodal(request, username):
    profiles = User.objects.get(username=username).profile.followers.all()
    context = {
        'profiles': profiles
    }
    return render(request, 'followers-modal.html', context)


def followingmodal(request, username):
    profiles = User.objects.get(username=username).profile.following.all()
    context = {
        'profiles': profiles
    }
    return render(request, 'followers-modal.html', context)
