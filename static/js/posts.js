$( document ).ready(function() {


    // $(document).on('click', '.carousel-control', function(e) {
    //     e.preventDefault();
    //     $('#myCarousel').carousel( $(this).data() );
    // });

    $(document).on('click', '.carousel-control-prev', function(e) {
        e.preventDefault();
        const id = e.currentTarget.id.replace('carouselPrev_', '');
        $(`#carouselExampleControls_${id}`).carousel('prev');
    });

    $(document).on('click', '.carousel-control-next', function(e) {
        e.preventDefault();
        const id = e.currentTarget.id.replace('carouselNext_', '');
        $(`#carouselExampleControls_${id}`).carousel('next');
    });

    $(document).on('submit', '.like-form', function(e) {
        e.preventDefault();
        const post_id = $(this).attr('id');
        const url = $(this).attr('action');
        const output = $(`#post__likes${post_id}`).get(0);
        const likebtn = $(`#likebtn_${post_id}`);

        $.ajax({
            type: 'POST',
            url: url,
            data: {
                'csrfmiddlewaretoken': $('input[name=csrfmiddlewaretoken]').val(),
                'post_id': post_id
            },
            success: function (response) {
                output.innerHTML = response;

                const form = $(`#likesmodalbutton_${post_id}`).get(0);
                let likes = 0;

                if ($(output).find('.avatars__one').length > 0) {
                    form.innerHTML = `<span>Liked by <b><span id="like_count{{ post.id }}" style="font-weight: bold">1</span> person</b></span>`;
                } else if ($(output).find('.avatars__zero').length > 0) {
                    form.innerHTML = "Nobody liked this post yet";
                } else {
                    if ($(output).find('.avatars__two').length > 0) {
                        likes = parseInt($(output).find('.avatars__two').children().length);
                    } else {
                        likes = parseInt($(output).find('.avatars__three').children().length);
                    }
                    form.innerHTML = `<span>Liked by <b><span id="like_count{{ post.id }}" style="font-weight: bold">${likes}</span> persons</b></span>`;
                }

                if ($(likebtn).hasClass("liked")) {
                    likebtn.attr("viewBox", "0 0 24 24");
                    likebtn.children().attr("d", "M16.792 3.904A4.989 4.989 0 0121.5 9.122c0 3.072-2.652 4.959-5.197 7.222-2.512 2.243-3.865 3.469-4.303 3.752-.477-.309-2.143-1.823-4.303-3.752C5.141 14.072 2.5 12.167 2.5 9.122a4.989 4.989 0 014.708-5.218 4.21 4.21 0 013.675 1.941c.84 1.175.98 1.763 1.12 1.763s.278-.588 1.11-1.766a4.17 4.17 0 013.679-1.938m0-2a6.04 6.04 0 00-4.797 2.127 6.052 6.052 0 00-4.787-2.127A6.985 6.985 0 00.5 9.122c0 3.61 2.55 5.827 5.015 7.97.283.246.569.494.853.747l1.027.918a44.998 44.998 0 003.518 3.018 2 2 0 002.174 0 45.263 45.263 0 003.626-3.115l.922-.824c.293-.26.59-.519.885-.774 2.334-2.025 4.98-4.32 4.98-7.94a6.985 6.985 0 00-6.708-7.218z");
                    likebtn.removeClass("liked").addClass("notliked");
                } else {
                    likebtn.attr("viewBox", "0 0 48 48");
                    likebtn.children().attr("d", "M34.6 3.1c-4.5 0-7.9 1.8-10.6 5.6-2.7-3.7-6.1-5.5-10.6-5.5C6 3.1 0 9.6 0 17.6c0 7.3 5.4 12 10.6 16.5.6.5 1.3 1.1 1.9 1.7l2.3 2c4.4 3.9 6.6 5.9 7.6 6.5.5.3 1.1.5 1.6.5s1.1-.2 1.6-.5c1-.6 2.8-2.2 7.8-6.8l2-1.8c.7-.6 1.3-1.2 2-1.7C42.7 29.6 48 25 48 17.6c0-8-6-14.5-13.4-14.5z");
                    likebtn.removeClass("notliked").addClass("liked");
                }
            },
            error: function (response) {
                if (response.status === 401) {
                    window.location.replace('/user/sign-in');
                }
            }
        });
    });

    $(document).on('submit', '.save-form', function (e) {
        e.preventDefault();
        const post_id = $(this).attr('id');
        const url = $(this).attr('action');
        const savebtn = $(`#savebtn_${post_id}`);

        $.ajax({
            type: 'POST',
            url: url,
            data: {
                'csrfmiddlewaretoken': $('input[name=csrfmiddlewaretoken]').val(),
                'post_id': post_id
            },
            success: function (response) {
                if (response.value === 'True') {
                    savebtn.removeClass("notsaved").addClass("saved");
                    savebtn.children().attr("d", "M20 22a.999.999 0 01-.687-.273L12 14.815l-7.313 6.912A1 1 0 013 21V3a1 1 0 011-1h16a1 1 0 011 1v18a1 1 0 01-1 1z");
                } else {
                    savebtn.removeClass("saved").addClass("notsaved");
                    savebtn.children().attr("d", "M20 21 12 13.44 4 21 4 3 20 3 20 21z");
                }
            },
            error: function (response) {
                if (response.status === 401) {
                    window.location.replace('/user/sign-in');
                }
            }
        });
    });

    $(document).on('submit', '.likedby-form', function (e) {
        e.preventDefault();
        const post_id = $(this).attr('id');
        const url = $(this).attr('action')
        const output = $('#likedbycontent').get(0);


        $.ajax({
            type: 'GET',
            url: url,
            data: {
                'csrfmiddlewaretoken': $('input[name=csrfmiddlewaretoken]').val(),
                'post_id': post_id
            },
            dataType: 'html',
            success: function (response) {
                output.innerHTML = response;
            },
            error: function (response) {
                console.log(response);
            }
        });
    });

});