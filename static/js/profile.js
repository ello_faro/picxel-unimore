$( document ).ready(function() {

    $(".followers-form").submit(function (e) {
        e.preventDefault();
        const url = $(this).attr('action')
        const output = $('#followerscontent').get(0);

        $.ajax({
            type: 'GET',
            url: url,
            data: {
                'csrfmiddlewaretoken': $('input[name=csrfmiddlewaretoken]').val()
            },
            dataType: 'html',
            success: function (response) {
                console.log(response);
                output.innerHTML = response;
            },
            error: function (response) {
                console.log(response);
            }
        });
    });
});

$( document ).ready(function() {

    $(".following-form").submit(function (e) {
        e.preventDefault();
        const url = $(this).attr('action')
        const output = $('#followerscontent').get(0);

        $.ajax({
            type: 'GET',
            url: url,
            data: {
                'csrfmiddlewaretoken': $('input[name=csrfmiddlewaretoken]').val()
            },
            dataType: 'html',
            success: function (response) {
                console.log(response);
                output.innerHTML = response;
            },
            error: function (response) {
                console.log(response);
            }
        });
    });
});