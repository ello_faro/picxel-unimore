$(document).ready(function () {

    const ctxLikes = document.getElementById('myLikesChart').getContext('2d');

    const myLikesChart = new Chart(ctxLikes, {
        type: 'line',
        data: {
            datasets: [{
                label: 'Likes received',
                data: JSON.parse(($('#likesData')[0].value).replace(/'/g, '"')),
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                y: {
                    beginAtZero: true,
                    ticks: {
                        precision: 0
                    }
                }
            }
        }
    });

    const ctxFollowers = document.getElementById('myFollowersChart').getContext('2d');

    const myFollowersChart = new Chart(ctxFollowers, {
        type: 'line',
        data: {
            datasets: [{
                label: 'Followers gained',
                data: JSON.parse(($('#followersData')[0].value).replace(/'/g, '"')),
                backgroundColor: [
                    'rgba(54, 162, 235, 0.2)',
                ],
                borderColor: [
                    'rgba(54, 162, 235, 1)',
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                y: {
                    beginAtZero: true,
                    ticks: {
                        precision: 0
                    }
                }
            }
        }
    });

    const ctxPosts = document.getElementById('myPostsChart').getContext('2d');

    const myPostsChart = new Chart(ctxPosts, {
        type: 'line',
        data: {
            datasets: [{
                label: 'Posts created',
                data: JSON.parse(($('#postsData')[0].value).replace(/'/g, '"')),
                backgroundColor: [
                    'rgba(255, 206, 86, 0.2)',
                ],
                borderColor: [
                    'rgba(255, 206, 86, 1)',
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                y: {
                    beginAtZero: true,
                    ticks: {
                        precision: 0
                    }
                }
            }
        }
    });

    const ctxFollowing = document.getElementById('myFollowingChart').getContext('2d');

    const myFollowingChart = new Chart(ctxFollowing, {
        type: 'line',
        data: {
            datasets: [{
                label: 'Account you started following',
                data: JSON.parse(($('#followingData')[0].value).replace(/'/g, '"')),
                backgroundColor: [
                    'rgba(175, 255, 103, 0.2)'
                ],
                borderColor: [
                    'rgba(131, 211, 59, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                y: {
                    beginAtZero: true,
                    ticks: {
                        precision: 0
                    }
                }
            }
        }
    });

});
