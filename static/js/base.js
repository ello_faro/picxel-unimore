$( document ).ready(function() {

    //Funzione per bottone menu mobile
    $('.second-button').on('click', function () {
        $('.animated-icon2').toggleClass('open');
    });

    $('#close-notifications').on('click', function () {
        $('.inner-dropdown').addClass('hidden');
    });

    //Funzione per tendina notifiche
    $(".notification-form").submit(function(e){
        e.preventDefault();
        const url = $(this).attr('action');
        const user = $(this).attr('id');
        const output = $('#output').get(0);
        const badge = $('.unread_notification_container')

        if(output.classList.contains("hidden")){
            $.ajax({
                type: 'GET',
                url: url,
                data: {
                    'csrfmiddlewaretoken': $('input[name=csrfmiddlewaretoken]').val(),
                    'user': user
                },
                dataType: 'html',
                success: function(response) {
                    output.innerHTML = response;
                    $(output).removeClass("hidden");
                    $(badge).addClass("hidden");
                },
                error: function(response) {
                    console.log("error", response)
                }
            });
        }
        else {
            $(output).addClass("hidden");
        }
    });

    //Funzione per ricerca utenti
    $("#searchuserbar").keyup(function() {
        let searchTerm = $(this)[0].value;
        const searchResults = $('#search-results');
        if(searchTerm.length === 0){
            searchResults.addClass("hidden");
        }
        else {
            searchResults.removeClass("hidden");

            $.ajax({
                type: 'GET',
                url: '/user/search',
                data: {
                    'q': searchTerm
                },
                success: function (data) {
                    searchResults.empty();
                    if (data.length !== 0) {
                        data.forEach((user) => {
                            let searchList = $(
                                `<a class="d-flex align-items-center dropdown-item" href="${'/user/' + user.username}">
                                        <div class="post__avatar">
                                            <img src="${user.img}">
                                        </div>
                                        <div class="d-flex flex-column">
                                            <span class="dropdown-name">${user.name}</span>
                                            <span class="dropdown-username">${"@" + user.username}</span>
                                        </div>    
                                  </a>`
                            );
                            searchList.appendTo(searchResults);
                        });
                    }
                    else {
                        let searchList = $(
                            `<div class="d-flex align-items-center dropdown-item">   
                                 <span>No results found...</span>
                            </div>`
                        );
                        searchList.appendTo(searchResults);
                    }
                }
            });
        }
    });

});