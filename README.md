# Picxel

Picxel è un social media creato con Django. Consente la pubblicazione di immagini e lo scambio di messaggi.

## Requisiti
Per questo progetto sono richiesti Python 3.10 e pip 22.2.2.

## Installazione
Prima di procedere, assicurarsi di avere pipenv installato. In caso contrario, lanciare il seguente comando:
```bash
pip install pipenv
```
Una volta fatto ciò, è necessario clonare la repository sul proprio computer.  
Successivamente, bisogna recarsi nella directory che contiene il progetto, e lanciare il seguente comando:

```bash
pipenv shell
```
In questo modo verrà creato un ambiente virtuale.  
Per installare le dipendenze necessarie in questo ambiente virtuale, lanciare il seguente comando:
```bash
pipenv install
```  
Al termine dell'installazione basterà avviare il server Django con:
```bash
python manage.py runserver
```


## Utilizzo

Nel database sono presenti 10 utenti e un utente staff (utilizzabile per consultare il database ma non per navigare all'interno del sito). 

Per effettuare il login, è sufficiente recarsi alla pagina di accesso e ricopiare uno degli username **(senza la @)** elencati qui sotto all'interno del campo "username" del form.  
La password è identica per tutti ed è **123test123**

Di seguito è riportata la lista degli username con cui si può effettuare l'accesso al sito:

@Threangster83  
@Makince  
@Awasine  
@Liffeent  
@Lusake  
@Whoul1994  
@Expless1954  
@Couspit  
@Hielturry  
@Thasarl94  
 

**Utente per accedere all'area admin:**   
Username: elia  
Password: admin
