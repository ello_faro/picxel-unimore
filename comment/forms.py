from django import forms
from comment.models import Comment


class NewComment(forms.ModelForm):
    body = forms.CharField(widget=forms.Textarea(attrs={
        'class': 'comment-textarea',
        'placeholder': 'Add a comment...',
        'autocomplete': 'off',
        'autocorrect': 'off'
    }), required=True)

    class Meta:
        model = Comment
        fields = ['body']
