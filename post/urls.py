from django.urls import path
from post.views import HomeView, CreatePostView,  PostDetailView,  PostByTagView, likepost, favourite, likedbymodal


urlpatterns = [
    path('', HomeView.as_view(), name="index"),
    path('newpost/', CreatePostView.as_view(), name="newpost"),
    path('<uuid:post_id>/', PostDetailView.as_view(), name="post-detail"),
    path('tag/<str:tag_title>', PostByTagView.as_view(), name="tags"),
    path('<uuid:post_id>/like', likepost, name="post-like"),
    path('<uuid:post_id>/favourite', favourite, name="post-favourite"),
    path('<uuid:post_id>/likedby', likedbymodal, name="post-likedby")
]
