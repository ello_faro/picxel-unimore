import uuid
from django.db import models
from django.contrib.auth.models import User
from django.template.defaultfilters import truncatechars
from django.utils.text import slugify
from django.urls import reverse
from notifications.models import Notification

# Create your models here.


# Upload dei file dell'utente in una directory specifica
def user_directory_path(instance, filename):
    return 'user_{0}/{1}'.format(instance.user.id, filename)


class Tag(models.Model):
    title = models.CharField(max_length=100, verbose_name="Tag", null=False)
    slug = models.SlugField(null=False, unique=True, default=uuid.uuid1)

    class Meta:
        verbose_name = 'Tag'
        verbose_name_plural = 'Tags'

    def get_absolute_url(self):
        return reverse('tags', args=[self.title])
    
    def __str__(self):
        return self.title
    
    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.slug)
        return super().save(*args, **kwargs)


class PostContent(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='content_owner')
    file = models.FileField(upload_to=user_directory_path)

    def __str__(self):
        return self.file.name


class Post(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    content = models.ManyToManyField(PostContent, related_name="content")
    caption = models.CharField(max_length=2000, verbose_name="Caption", blank=False)
    date = models.DateTimeField(auto_now_add=True)
    tag = models.ManyToManyField(Tag, related_name="tags", blank=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    liked_by = models.ManyToManyField(User, related_name="likes", blank=True)
    likes = models.IntegerField(default=0)

    @property
    def short_caption(self):
        return truncatechars(self.caption, 20)

    # URI per accedere ai dettagli del post
    def get_absolute_url(self):
        return reverse('post-detail', args=[str(self.id)])

    def __str__(self):
        return self.caption
