from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, HTML, Field
from django import forms
from post.models import Post


class NewPost(forms.ModelForm):
    content = forms.FileField(widget=forms.ClearableFileInput(attrs={'multiple': True}), required=True)
    caption = forms.CharField(widget=forms.Textarea(attrs={'class': 'input', 'placeholder': 'Caption'}),
                              required=True)
    tag = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'input', 'placeholder': 'Tags (separate with comma)'}), required=False)

    def __init__(self, *args, **kwargs):
        super(NewPost, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.layout = Layout(Field('content', 'caption', 'tag'),
                                    HTML("""<button class="btn btn-success btn-lg mt-5" type='submit'>Create post</button>"""))

    class Meta:
        model = Post
        fields = ['content', 'caption', 'tag']
