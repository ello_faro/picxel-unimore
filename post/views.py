from datetime import date

import sweetify
from django.shortcuts import render, redirect
from django.http import JsonResponse, HttpResponseRedirect
from django.utils.decorators import method_decorator
from django.views.generic import ListView, FormView, DetailView
from django.views.generic.edit import FormMixin

from comment.forms import NewComment
from comment.models import Comment
from notifications.models import Notification, Statistics
from post.models import Tag, Post, PostContent
from post.forms import NewPost
from django.urls import reverse
from userauth.decorators import ajax_login_required, complete_profile_or_anonymous_user_required, deny_anonymous_user
from userauth.models import Profile


# Create your views here.

@method_decorator(complete_profile_or_anonymous_user_required, name='dispatch')
class HomeView(ListView):
    model = Post
    paginate_by = 3
    context_object_name = 'posts'
    template_name = 'index.html'

    def get_queryset(self):
        if self.request.user.is_anonymous:
            qs = super().get_queryset()
            return qs.order_by('-date')
        else:
            qs = super().get_queryset()
            users_list = self.request.user.profile.following.all()
            return qs.filter(user__in=users_list).order_by('-date')

    def get_context_data(self, **kwargs):
        if self.request.user.is_anonymous:
            context = super(HomeView, self).get_context_data(**kwargs)
            context['logged_user'] = 'guest'
        else:
            context = super(HomeView, self).get_context_data(**kwargs)
            context['logged_user'] = self.request.user
            context['suggested_users'] = Profile.objects.all()[:6]
        return context


@method_decorator(deny_anonymous_user, name='dispatch')
class CreatePostView(FormView):
    template_name = 'newpost.html'
    form_class = NewPost

    def post(self, request, *args, **kwargs):
        form = NewPost(request.POST, request.FILES)
        request_files = request.FILES.getlist('content')

        for f in request_files:
            if not f.name.endswith(('.mp4', '.png', '.jpg')):
                sweetify.error(self.request, 'Error!', text='You uploaded a file with a non-allowed extension. '
                                                            'Only .mp4, .jpg and .png are allowed.', persistent='Retry')
                return self.form_invalid(form)

        if form.is_valid():
            return self.form_valid(form, request_files)
        else:
            return self.form_invalid(form)

    def form_valid(self, form, *args):
        post_tags = form.cleaned_data.get('tag').replace(", ", ',')
        tag_list = list(post_tags.split(','))

        form_tags = []
        for tag in tag_list:
            if tag != '':
                t, created = Tag.objects.get_or_create(title=tag)
                form_tags.append(t)

        form_files = []
        for f in args[0]:
            file_instance = PostContent.objects.create(user=self.request.user, file=f)
            form_files.append(file_instance)

        post = Post.objects.create(user=self.request.user, caption=form.cleaned_data.get('caption'))
        post.content.set(form_files)
        post.tag.set(form_tags)
        post.save()

        Statistics.objects.create(sender=self.request.user, user=self.request.user, type=3)
        return HttpResponseRedirect(reverse('profile', args=[self.request.user.username]))


@method_decorator(complete_profile_or_anonymous_user_required, name='dispatch')
class PostDetailView(FormMixin, DetailView):
    template_name = 'postdetails.html'
    model = Post
    form_class = NewComment

    def get_success_url(self):
        return reverse('post-detail', kwargs={'post_id': self.kwargs['post_id']})

    def get_object(self, *args):
        return Post.objects.get(pk=self.kwargs['post_id'])

    def get_context_data(self, *args, **kwargs):
        context = super(PostDetailView, self).get_context_data(**kwargs)
        context["comments"] = Comment.objects.filter(post=context['object']).order_by("-date")
        context['form'] = NewComment()
        context['logged_user'] = self.request.user
        return context

    def post(self, request, *args, **kwargs):
        form = NewComment(request.POST)
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form):
        form = form.save(commit=False)
        form.user = self.request.user
        form.post = Post.objects.get(pk=self.kwargs['post_id'])
        form.save()
        if form.user != form.post.user:
            Notification.objects.create(post=form.post, sender=form.user, user=form.post.user, type=2, body=form.body)
        return super().form_valid(form)

    def form_invalid(self, form):
        return redirect(reverse('post-detail', kwargs={'post_id': self.kwargs['post_id']}))


@method_decorator(complete_profile_or_anonymous_user_required, name='dispatch')
class PostByTagView(ListView):
    model = Post
    template_name = 'postbytag.html'

    def get_queryset(self):
        tag = Tag.objects.get(title=self.kwargs['tag_title'])
        qs = Post.objects.filter(tag=tag).order_by('-date')
        return qs

    def get_context_data(self, **kwargs):
        context = super(PostByTagView, self).get_context_data(**kwargs)
        context['tag'] = self.kwargs['tag_title']
        return context


@ajax_login_required
def likepost(request, post_id):
    logged_user = request.user
    post = Post.objects.get(id=post_id)

    # Controllo se l'utente ha già messo like al post
    if post.liked_by.filter(id=logged_user.id).exists():
        post.liked_by.remove(logged_user)
    else:
        post.liked_by.add(logged_user)

    if post.user != logged_user:
        notification = Notification.objects.filter(post=post, sender=logged_user, user=post.user, type=1)
        if notification.count() == 0:
            Notification.objects.create(post=post, sender=logged_user, user=post.user, type=1)
            if Statistics.objects.filter(post=post, sender=logged_user, user=post.user, type=1, date=date.today()).count() == 0:
                Statistics.objects.create(post=post, sender=logged_user, user=post.user, type=1)
        else:
            Notification.objects.filter(post=post, sender=logged_user, user=post.user, type=1).delete()

    post.likes = post.liked_by.all().count()
    post.save()

    context = {
        'likes': post.liked_by.all().count(),
        'users': post.liked_by.all()
    }

    return render(request, 'likedby.html', context)


@ajax_login_required
def favourite(request, post_id):
    logged_user = request.user
    post = Post.objects.get(id=post_id)
    profile = Profile.objects.get(user=logged_user)

    # Controllo se l'utente ha già salvato il post
    if profile.favourites.filter(id=post_id).exists():
        profile.favourites.remove(post)
        saved = 'False'
    else:
        profile.favourites.add(post)
        saved = 'True'

    post.save()

    data = {
        'value': saved
    }

    return JsonResponse(data, safe=False)


def likedbymodal(request, post_id):
    post = Post.objects.get(id=post_id)
    profiles = post.liked_by.all()

    context = {
        'profiles': profiles
    }

    return render(request, 'likes-modal.html', context)
