import logging
import unittest
import uuid

from django.contrib import auth
from django.test import TestCase
from django.urls import reverse
from django.core.files.uploadedfile import SimpleUploadedFile

from comment.models import Comment
from notifications.models import Notification
from post.forms import NewPost
from post.models import Post, User, PostContent, Tag
from userauth.models import Profile

logger = logging.getLogger(__name__)
logging.disable(logging.NOTSET)
logger.setLevel(logging.DEBUG)


# Create your tests here.


class PostCreationTests(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(username='testuser', password='12345')
        file = SimpleUploadedFile("file.png", b"file_content", content_type="image/png")
        postcontent = PostContent.objects.create(user=self.user, file=file)
        self.p = Post(id=uuid.uuid4(), user=self.user, caption="test_post")
        self.p.content.add(postcontent)
        self.p.save()

    # consistenza architetturale
    def testObj(self):
        self.assertTrue(
            isinstance(self.p, Post),
            "p non è un Post"
        )

    # coerenza funzionale: metodi invocabili
    def test_methods(self):
        self.assertTrue(
            callable(self.p.get_absolute_url),
            "p non ha il metodo get_absolute_url"
        )
        self.assertTrue(
            callable(self.p.__str__),
            "p non ha il metodo __str__"
        )

    def test_post_without_content(self):
        # robustezza: input non validi
        self.assertTrue(
            self.p.content.exists(),
            "Manca il contenuto del post"
        )

    # coerenza funzionale: risultati attesi dei metodi get/set
    def test_method_str(self):
        self.assertEqual(
            self.p.__str__(),
            "test_post",
            "La didascalia del post non è stata recuperata correttamente"
        )

    # coerenza funzionale: risultati attesi dei metodi
    def test_method_absolute_url(self):
        self.assertEqual(
            self.client.get(self.p.get_absolute_url()).status_code,
            200,
            "L'URL assoluto non viene composto correttamente"
        )

    def tearDown(self):
        del self.p


class PostViewsTest(TestCase):

    def setUp(self):
        self.user = User.objects.create_user(username='testuser', password='12345')
        self.follower = User.objects.create_user(username='follower', password='67890')

        self.testprofile = Profile.objects.create(user=self.user, first_name="profile", last_name="logged", email="testmail@test.it")
        self.followerprofile = Profile.objects.create(user=self.follower, first_name="follower", last_name="test")

        self.testprofile.followers.add(self.follower)
        self.followerprofile.following.add(self.user)

        self.file = SimpleUploadedFile("file.png", b"file_content", content_type="image/png")
        postcontent = PostContent.objects.create(user=self.user, file=self.file)

        tag = Tag.objects.create(title="tag")
        self.t = tag

        self.p = Post(id=uuid.uuid4(), user=self.user, caption="test_post")
        self.p.content.add(postcontent)
        self.p.tag.add(tag)

        self.p.save()

    def test_navigate_to_404(self):
        self.assertEqual(
            self.client.get('04324-239402-124923-49281').status_code,
            404,
            "La risposta non è 404"
        )

    def test_get_posts_for_logged_user(self):
        login = self.client.login(username="follower", password="67890")
        self.assertTrue(login)
        response = self.client.get(reverse('index'))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.context['object_list']), 1)

    def test_get_posts_for_anonymous_user(self):
        user = auth.get_user(self.client)
        self.assertTrue(user.is_anonymous)
        response = self.client.get(reverse('index'))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.context['object_list']), 1)

    def test_create_new_post_valid_form(self):
        login = self.client.login(username="testuser", password="12345")
        self.assertTrue(login)
        data = {
            "user": self.user,
            "content": self.file,
            "caption": "testcaption",
            "tag": "testtag"
        }
        form = NewPost(data={"user": self.user, "caption": "testcaption", "tag": "testtag"}, files={"content": self.file})
        self.assertEqual(form.is_valid(), True)
        self.client.post(reverse('newpost'), data=data)
        self.assertEqual(Post.objects.filter(user=self.user).count(), 1)

    def test_create_new_post_without_content(self):
        login = self.client.login(username="testuser", password="12345")
        self.assertTrue(login)
        data = {
            "content": "",
            "caption": "testcaption",
            "tag": "testtag"
        }
        form = NewPost(data={"caption": "testcaption", "tag": "testtag"}, files={})
        self.assertEqual(form.is_valid(), False)
        response = self.client.post(reverse('newpost'), data=data)
        self.assertTrue(response, 200)

    def test_get_post_details(self):
        self.assertEqual(
            self.client.get(reverse('post-detail', args=[self.p.id])).status_code,
            200,
            "La risposta non è 200"
        )

        self.assertEqual(
            self.client.get(self.p.get_absolute_url()).status_code,
            200,
            "La risposta non è 200"
        )

    def test_get_post_by_tag(self):
        response = self.client.get(reverse('tags', args=[self.t]))
        self.assertEqual(
            response.status_code,
            200,
            "La risposta non è 200"
        )
        self.assertEqual(len(response.context['object_list']), 1)

    def test_comment_post_valid_form(self):
        login = self.client.login(username="follower", password="67890")
        self.assertTrue(login)
        data = {'body': 'hello world'}
        response = self.client.post((reverse('post-detail', args=[self.p.id])), data=data)
        self.assertEqual(
            response.status_code,
            302,
            "Errore nella pubblicazione del commento"
        )
        self.assertTrue(Comment.objects.filter(post=self.p, user=self.follower, body=data['body']).exists(), 1)
        self.assertTrue(Notification.objects.filter(post=self.p, user=self.p.user, sender=self.follower, type=2, body='hello world').exists(), 1)

    def test_like_post_ajax_logged_user(self):
        self.client.login(username="follower", password="67890")
        response = self.client.get(reverse('post-like', args=[self.p.id]))
        self.assertEqual(
            response.status_code,
            200,
            "Lo status deve essere 200"
        )
        self.assertEqual(response.context['likes'], 1)
        self.assertTrue(Notification.objects.filter(post=self.p, user=self.p.user, sender=self.follower, type=1).exists(), 1)

    def test_save_post_ajax_logged_user(self):
        self.client.login(username="testuser", password="12345")
        response = self.client.get(reverse('post-favourite', args=[self.p.id]))
        self.assertEqual(
            response.status_code,
            200,
            "Lo status deve essere 200"
        )

    def test_like_post_ajax_anonymous_user(self):
        response = self.client.get(reverse('post-like', args=[self.p.id]))
        self.assertEqual(
            response.status_code,
            401,
            "Lo status deve essere 401"
        )

    def test_save_post_ajax_anonymous_user(self):
        response = self.client.get(reverse('post-favourite', args=[self.p.id]))
        self.assertEqual(
            response.status_code,
            401,
            "Lo status deve essere 401"
        )

    def tearDown(self):
        del self.p


if __name__ == '__main__':
    unittest.main()
