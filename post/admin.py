from django.contrib import admin
from post.models import Tag, Post, PostContent


# Register your models here.

class SettingAdmin(admin.ModelAdmin):
    list_display = ('short_caption', 'user', 'id')


admin.site.register(Tag)
admin.site.register(Post, SettingAdmin)
admin.site.register(PostContent)
