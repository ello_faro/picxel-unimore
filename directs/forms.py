from django import forms
from django.contrib.auth.models import User

from directs.models import Message


class NewMessage(forms.ModelForm):
    user = forms.ModelChoiceField(queryset=User.objects.all(), required=True)
    sender = forms.ModelChoiceField(queryset=User.objects.all(), required=True)
    receiver = forms.ModelChoiceField(queryset=User.objects.all(), required=True)
    body = forms.CharField(widget=forms.Textarea(attrs={
        'class': 'message-textarea',
        'placeholder': 'Send message...',
        'autocomplete': 'off',
        'autocorrect': 'off'
    }), required=True)
    is_read = forms.BooleanField(required=False)

    class Meta:
        model = Message
        fields = ['user', 'sender', 'receiver', 'body', 'is_read']
