from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import reverse

from directs.forms import NewMessage
from directs.models import Message
from userauth.models import Profile


# Create your tests here.

class DirectCreationTests(TestCase):
    def setUp(self):
        self.sender = User.objects.create_user(username='testsender', password='12345')
        self.receiver = User.objects.create_user(username='testreceiver', password='67890')
        self.direct = Message.objects.create(user=self.sender, sender=self.sender, receiver=self.receiver, body='hello')

    # consistenza architetturale
    def testObj(self):
        self.assertTrue(
            isinstance(self.direct, Message),
            "direct non è un Message"
        )

    # coerenza funzionale: metodi invocabili
    def test_methods(self):
        self.assertTrue(
            callable(self.direct.send_message),
            "direct non ha il metodo send_message"
        )

    def test_direct_without_body(self):
        # robustezza: input non validi
        self.assertTrue(
            self.direct.body != '',
            "Manca il corpo del direct"
        )

    def tearDown(self):
        del self.direct


class MessageViewsTest(TestCase):
    def setUp(self):
        self.sender = User.objects.create_user(username='testsender', password='12345')
        self.profilesender = Profile.objects.create(user=self.sender, first_name="test", last_name="sender", email="test@testsender.com")

        self.receiver = User.objects.create_user(username='testreceiver', password='67890')
        self.profilereceiver = Profile.objects.create(user=self.receiver, first_name="test", last_name="receiver", email="test@testreceiver.com")

    def test_send_valid_message(self):
        login = self.client.login(username="testsender", password="12345")
        self.assertTrue(login)
        data = {
            "user": self.sender,
            "sender": self.sender,
            "receiver": self.receiver,
            "body": "hello"
        }
        form = NewMessage(data=data)
        self.assertEqual(form.is_valid(), True)
        self.client.post(reverse('directs', args=[self.receiver.username]), data=data)
        self.assertEqual(Message.objects.filter(user=self.sender, sender=self.sender, receiver=self.receiver, body='hello').count(), 1)
        self.assertEqual(Message.objects.filter(user=self.receiver, sender=self.sender, receiver=self.sender, body='hello').count(), 1)