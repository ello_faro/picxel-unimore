from django.contrib.auth.models import User
from django.shortcuts import render, redirect
from django.utils.decorators import method_decorator
from django.views import View
from django.views.generic import ListView

from directs.forms import NewMessage
from directs.models import Message
from django.core.paginator import Paginator
from django.db.models import Q, Max, ExpressionWrapper, BooleanField, Value, Case, When, Func, F
from userauth.decorators import deny_anonymous_user
from userauth.models import Profile


# Create your views here.

@method_decorator(deny_anonymous_user, name='dispatch')
class InboxView(ListView):
    model = User
    template_name = 'directs/inbox.html'

    def get_queryset(self):
        qs = get_chat_list(self.request.user)
        return qs


def get_chat_list(user):
    users = []
    messages = Message.objects.filter(user=user).values("receiver").annotate(last=Max('date')).order_by("-last")
    for message in messages:
        users.append({
            'user': User.objects.get(pk=message['receiver']),
            'last': message['last'],
            'unread': Message.objects.filter(user=user, receiver__pk=message['receiver'], is_read=False).count()
        })
    return users


@method_decorator(deny_anonymous_user, name='dispatch')
class DirectsView(View):
    def get(self, request, username):
        logged_user = request.user
        chats = get_chat_list(user=logged_user)
        receiver = User.objects.get(username=username)
        direct_list = Message.objects.filter(user=logged_user, receiver__username=username)
        direct_list.update(is_read=True)

        for chat in chats:
            if chat['user'].username == receiver:
                chat['unread'] = 0

        form = NewMessage()

        context = {
          'form': form,
          'directs': direct_list,
          'active_direct': username,
          'chats': chats
        }

        return render(request, 'directs/directs.html', context)

    def post(self, request, username):
        senderform = NewMessage(data={
            'user': request.user,
            'sender': request.user,
            'receiver': User.objects.get(username=username),
            'body': request.POST.getlist('body')[0],
            'is_read': True})

        receiverform = NewMessage(data={
            'user': User.objects.get(username=username),
            'sender': request.user,
            'receiver': request.user,
            'body': request.POST.getlist('body')[0],
            'is_read': False})

        if senderform.is_valid() and receiverform.is_valid():
            senderform.save()
            receiverform.save()

        return redirect("/message/directs/" + username)


@method_decorator(deny_anonymous_user, name='dispatch')
class UserSearchView(ListView):
    paginate_by = 5
    model = User
    template_name = 'directs/search.html'

    def querystring(self):
        qs = self.request.GET.copy()
        qs.pop(self.page_kwarg, None)
        return qs.urlencode()

    def get_queryset(self):
        qs = super().get_queryset().filter(~Q(username=self.request.user.username)).filter(profile__isnull=False)
        print(self.request.user.profile)
        if 'q' in self.request.GET:
            qs = qs.filter(username__icontains=self.request.GET['q'])
        return qs.order_by('username')


@deny_anonymous_user
def newmessage(request, username):
    try:
        to_user = User.objects.get(username=username)
    except Exception as e:
        return redirect('usersearch')
    return redirect("/message/directs/" + to_user.username)


def unread_chats(request):
    chat_count = 0
    if request.user.is_authenticated:
        chat_count = Message.objects.all().values('sender').filter(user=request.user, is_read=False).distinct().count()

    return {'chat_count': chat_count}
