from django.urls import path
from directs.views import newmessage, DirectsView, InboxView, UserSearchView

urlpatterns = [

    path('inbox/', InboxView.as_view(), name="inbox"),
    path('directs/<username>', DirectsView.as_view(), name="directs"),
    path('search/', UserSearchView.as_view(), name="usersearch"),
    path('newmessage/<username>', newmessage, name="newmessage")

]
