from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone


# Create your models here.

class Notification(models.Model):
    NOTIFICATION_TYPES = ((1, 'like'), (2, 'comment'), (3, 'follow'))

    post = models.ForeignKey('post.Post', on_delete=models.CASCADE, related_name="post_notification", blank=True, null=True)
    sender = models.ForeignKey(User, on_delete=models.CASCADE, related_name="notification_sender")
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="notification_receiver")
    type = models.IntegerField(choices=NOTIFICATION_TYPES)
    body = models.CharField(max_length=90, blank=True)
    date = models.DateTimeField(auto_now_add=True)
    is_read = models.BooleanField(default=False)

class Statistics(models.Model):
    STATS_TYPES = ((1, 'like'), (2, 'follow'), (3, 'postcreation'))

    post = models.ForeignKey('post.Post', on_delete=models.CASCADE, related_name="post", blank=True, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="stats_user")
    sender = models.ForeignKey(User, on_delete=models.CASCADE, related_name="stats_sender")
    type = models.IntegerField(choices=STATS_TYPES)
    date = models.DateField(default=timezone.now)

    class Meta:
        verbose_name_plural = "Statistics"