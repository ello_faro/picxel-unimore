from django.contrib import admin
from notifications.models import Notification, Statistics


# Register your models here.

class Filter(admin.ModelAdmin):
    list_filter = ('user', 'type')
    readonly_fields = ('date',)


class DateObj(admin.ModelAdmin):
    readonly_fields = ('date',)


admin.site.register(Notification, Filter)
admin.site.register(Statistics, DateObj)
