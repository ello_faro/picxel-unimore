from django.urls import path
from notifications.views import ShowNotificationsListView, ShowNotificationsView, RemoveNotificationView

urlpatterns = [
    path('', ShowNotificationsView.as_view(), name="notifications"),
    path('all', ShowNotificationsListView.as_view(), name="notifications_list"),
    path('<int:pk>/remove', RemoveNotificationView.as_view(), name="remove_notification")
]
