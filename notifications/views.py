from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic import ListView, UpdateView, DeleteView
from notifications.models import Notification
from userauth.decorators import deny_anonymous_user


# Create your views here.

@method_decorator(deny_anonymous_user, name='dispatch')
class ShowNotificationsView(ListView):
    paginate_by = 5
    model = Notification
    template_name = 'notifications.html'

    def get_queryset(self):
        qs = super().get_queryset()
        Notification.objects.filter(user=self.request.user, is_read=False).update(is_read=True)
        return qs.filter(user=self.request.user).order_by('-date')


@method_decorator(deny_anonymous_user, name='dispatch')
class ShowNotificationsListView(ListView):
    paginate_by = 5
    model = Notification
    template_name = 'notifications-list.html'

    def get_queryset(self):
        qs = super().get_queryset()
        Notification.objects.filter(user=self.request.user, is_read=False).update(is_read=True)
        return qs.filter(user=self.request.user).order_by('-date')


class RemoveNotificationView(DeleteView):
    model = Notification
    success_url = reverse_lazy('notifications_list')


def unread_notifications(request):
    notifications_count = 0
    if request.user.is_authenticated:
        notifications_count = Notification.objects.all().filter(user=request.user, is_read=False).count()
    return {'notifications_count': notifications_count}
